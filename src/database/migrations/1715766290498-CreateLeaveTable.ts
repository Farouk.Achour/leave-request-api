import { MigrationInterface, QueryRunner } from "typeorm";

export class CreateLeaveTable1715766290498 implements MigrationInterface {
    name = 'CreateLeaveTable1715766290498'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`CREATE TABLE "leave" ("id" SERIAL NOT NULL, "type" character varying NOT NULL, "description" character varying NOT NULL, "from" TIMESTAMP NOT NULL, "to" TIMESTAMP NOT NULL, "status" character varying NOT NULL, "userId" integer, CONSTRAINT "PK_501f6ea368365d2a40b1660e16b" PRIMARY KEY ("id"))`);
        await queryRunner.query(`ALTER TABLE "leave" ADD CONSTRAINT "FK_9fb20081bf48840a16e0d33d14e" FOREIGN KEY ("userId") REFERENCES "user"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "leave" DROP CONSTRAINT "FK_9fb20081bf48840a16e0d33d14e"`);
        await queryRunner.query(`DROP TABLE "leave"`);
    }

}
