import { MigrationInterface, QueryRunner } from "typeorm";

export class ChangeUsersTableAgain1715761082823 implements MigrationInterface {
    name = 'ChangeUsersTableAgain1715761082823'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "user" ADD "gender" character varying`);
        await queryRunner.query(`ALTER TABLE "user" ADD "birthday" character varying`);
        await queryRunner.query(`ALTER TABLE "user" ADD "address" character varying`);
        await queryRunner.query(`ALTER TABLE "user" ADD "phoneNumber1" character varying`);
        await queryRunner.query(`ALTER TABLE "user" ADD "phoneNumber2" character varying`);
        await queryRunner.query(`ALTER TABLE "user" ADD "job" character varying`);
        await queryRunner.query(`ALTER TABLE "user" ADD "department" character varying`);
        await queryRunner.query(`ALTER TABLE "user" ADD "hiringDate" character varying`);
        await queryRunner.query(`ALTER TABLE "user" ADD "leaveRate" character varying`);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "user" DROP COLUMN "leaveRate"`);
        await queryRunner.query(`ALTER TABLE "user" DROP COLUMN "hiringDate"`);
        await queryRunner.query(`ALTER TABLE "user" DROP COLUMN "department"`);
        await queryRunner.query(`ALTER TABLE "user" DROP COLUMN "job"`);
        await queryRunner.query(`ALTER TABLE "user" DROP COLUMN "phoneNumber2"`);
        await queryRunner.query(`ALTER TABLE "user" DROP COLUMN "phoneNumber1"`);
        await queryRunner.query(`ALTER TABLE "user" DROP COLUMN "address"`);
        await queryRunner.query(`ALTER TABLE "user" DROP COLUMN "birthday"`);
        await queryRunner.query(`ALTER TABLE "user" DROP COLUMN "gender"`);
    }

}
