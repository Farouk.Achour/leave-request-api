import { MigrationInterface, QueryRunner } from "typeorm";

export class CreateDepartmentTable1715934497525 implements MigrationInterface {
    name = 'CreateDepartmentTable1715934497525'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`CREATE TABLE "department" ("id" SERIAL NOT NULL, "name" character varying NOT NULL, "chief" character varying NOT NULL, "description" character varying NOT NULL, "photoId" uuid, CONSTRAINT "REL_43d0424ad4408356b81da0b725" UNIQUE ("photoId"), CONSTRAINT "PK_9a2213262c1593bffb581e382f5" PRIMARY KEY ("id"))`);
        await queryRunner.query(`ALTER TABLE "department" ADD CONSTRAINT "FK_43d0424ad4408356b81da0b725e" FOREIGN KEY ("photoId") REFERENCES "file"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "department" DROP CONSTRAINT "FK_43d0424ad4408356b81da0b725e"`);
        await queryRunner.query(`DROP TABLE "department"`);
    }

}
