import { MigrationInterface, QueryRunner } from "typeorm";

export class CreateHolidayTable1715941320856 implements MigrationInterface {
    name = 'CreateHolidayTable1715941320856'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`CREATE TABLE "holiday" ("id" SERIAL NOT NULL, "name" character varying NOT NULL, "date" TIMESTAMP NOT NULL, "duration" TIMESTAMP NOT NULL, "type" character varying NOT NULL, CONSTRAINT "PK_3e7492c25f80418a7aad0aec053" PRIMARY KEY ("id"))`);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`DROP TABLE "holiday"`);
    }

}
