import { MigrationInterface, QueryRunner } from "typeorm";

export class CreateHolidayTable1715942628195 implements MigrationInterface {
    name = 'CreateHolidayTable1715942628195'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "holiday" DROP COLUMN "duration"`);
        await queryRunner.query(`ALTER TABLE "holiday" ADD "duration" integer NOT NULL`);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "holiday" DROP COLUMN "duration"`);
        await queryRunner.query(`ALTER TABLE "holiday" ADD "duration" TIMESTAMP NOT NULL`);
    }

}
