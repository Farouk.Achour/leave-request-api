import { Exclude, Expose } from 'class-transformer';
import { FileType } from '../../files/domain/file';
import { Role } from '../../roles/domain/role';
import { Status } from '../../statuses/domain/status';
import { ApiResponseProperty } from '@nestjs/swagger';

const idType = Number;

export class User {
  @ApiResponseProperty({
    type: idType,
  })
  id: number | string;

  @ApiResponseProperty({
    type: String,
    example: 'john.doe@example.com',
  })
  @Expose({ groups: ['me', 'admin'] })
  email: string | null;

  @Exclude({ toPlainOnly: true })
  password?: string;

  @Exclude({ toPlainOnly: true })
  previousPassword?: string;

  @ApiResponseProperty({
    type: String,
    example: 'email',
  })
  @Expose({ groups: ['me', 'admin'] })
  provider: string;

  @ApiResponseProperty({
    type: String,
    example: '1234567890',
  })
  @Expose({ groups: ['me', 'admin'] })
  socialId?: string | null;

  @ApiResponseProperty({
    type: String,
    example: 'John',
  })
  fullName: string | null;

  @ApiResponseProperty({
    type: () => FileType,
  })
  photo?: FileType | null;

  @ApiResponseProperty({
    type: () => Role,
  })
  role?: Role | null;

  @Expose({ groups: ['me', 'admin'] })
  gender?: string | null;

  @Expose({ groups: ['me', 'admin'] })
  address?: string | null;

  @Expose({ groups: ['me', 'admin'] })
  birthday?: string | null;

  @Expose({ groups: ['me', 'admin'] })
  phoneNumber1?: string | null;

  @Expose({ groups: ['me', 'admin'] })
  phoneNumber2?: string | null;

  @Expose({ groups: ['me', 'admin'] })
  job?: string | null;

  @Expose({ groups: ['me', 'admin'] })
  department?: string | null;

  @ApiResponseProperty({
    type: () => Status,
  })
  status?: Status;

  @Expose({ groups: ['me', 'admin'] })
  hiringDate?: string | null;

  @Expose({ groups: ['me', 'admin'] })
  leaveRate?: number | null;

  @ApiResponseProperty()
  createdAt: Date;

  @ApiResponseProperty()
  updatedAt: Date;

  @ApiResponseProperty()
  deletedAt: Date;
}
