import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Leave } from './entities/leave.entity';
import { Repository } from 'typeorm';
import { CreateLeaveDto } from './dtos/create-leave.dto';
import { NotificationsGateway } from '../notifications/notifications.gateway';

@Injectable()
export class LeavesService {
  constructor(
    @InjectRepository(Leave)
    private readonly leavesRepository: Repository<Leave>,
    private readonly notificationsGateway: NotificationsGateway,
  ) {}

  async findLeaves() {
    const leaves = await this.leavesRepository.find();

    return leaves;
  }

  async findMyLeaves(id: any) {
    const leaves = await this.leavesRepository.find({
      where: {
        user: {
          id: id,
        },
      },
    });

    return leaves;
  }

  async createLeave(createLeaveDto: CreateLeaveDto) {
    const leave = await this.leavesRepository.create(createLeaveDto);

    this.notificationsGateway.server.emit('newLeaveRequest', { leave });

    return this.leavesRepository.save(leave);
  }

  async updateLeave(id: any, updateLeaveDto: any) {
    const clonedPayload = {
      id: updateLeaveDto.id,
      type: updateLeaveDto.type,
      description: updateLeaveDto.description,
      from: updateLeaveDto.from,
      to: updateLeaveDto.to,
      status: updateLeaveDto.status,
      user: updateLeaveDto.user,
    };
    const leave = await this.leavesRepository.update(id, clonedPayload);

    return leave;
  }
}
