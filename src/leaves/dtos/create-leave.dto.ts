import { IsDate, IsString } from 'class-validator';
import { UserEntity } from '../../users/infrastructure/persistence/relational/entities/user.entity';

export class CreateLeaveDto {
  @IsString()
  readonly type: string;

  @IsString()
  readonly description: string;

  @IsString()
  readonly from: string;

  @IsString()
  readonly to: string;

  @IsString()
  readonly status: string;

  @IsString()
  readonly user: UserEntity;
}
