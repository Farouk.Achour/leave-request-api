import { Column, Entity, ManyToOne, PrimaryGeneratedColumn } from 'typeorm';
import { EntityRelationalHelper } from '../../utils/relational-entity-helper';
import { UserEntity } from '../../users/infrastructure/persistence/relational/entities/user.entity';

@Entity({
  name: 'leave',
})
export class Leave extends EntityRelationalHelper {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  type: string;

  @Column()
  description: string;

  @Column()
  from: Date;

  @Column()
  to: Date;

  @Column()
  status: string;

  @ManyToOne(() => UserEntity, {
    eager: true,
  })
  user: UserEntity;
}
