import {
  Body,
  Controller,
  Get,
  Param,
  Patch,
  Post,
  UseGuards,
} from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';
import {
  ApiBearerAuth,
  ApiCreatedResponse,
  ApiParam,
  ApiTags,
} from '@nestjs/swagger';
import { RolesGuard } from '../roles/roles.guard';
import { Roles } from '../roles/roles.decorator';
import { RoleEnum } from '../roles/roles.enum';
import { LeavesService } from './leaves.service';
import { CreateLeaveDto } from './dtos/create-leave.dto';
import { Leave } from './entities/leave.entity';

@ApiBearerAuth()
@UseGuards(AuthGuard('jwt'), RolesGuard)
@ApiTags('leaves')
@Controller({
  path: 'leaves',
  version: '1',
})
export class LeavesController {
  constructor(private readonly leavesService: LeavesService) {}

  @Roles(RoleEnum.admin, RoleEnum.user)
  @Get()
  async getLeaves() {
    const leaves = await this.leavesService.findLeaves();

    return leaves;
  }

  @Roles(RoleEnum.admin, RoleEnum.user)
  @Get(':id')
  @ApiParam({
    name: 'id',
    type: String,
    required: true,
  })
  async getMyLeaves(@Param('id') id: any) {
    const leaves = await this.leavesService.findMyLeaves(id);

    return leaves;
  }

  @ApiCreatedResponse()
  @Roles(RoleEnum.admin, RoleEnum.user)
  @Post()
  async createLeave(@Body() body: any) {
    return this.leavesService.createLeave(body);
  }

  @ApiCreatedResponse()
  @Roles(RoleEnum.admin)
  @Patch(':id')
  @ApiParam({
    name: 'id',
    type: String,
    required: true,
  })
  async updateLeave(@Param('id') id: Leave['id'], @Body() body: any) {
    return this.leavesService.updateLeave(id, body);
  }
}
