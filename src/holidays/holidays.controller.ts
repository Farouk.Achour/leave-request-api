import {
  Body,
  Controller,
  Delete,
  Get,
  Param,
  Patch,
  Post,
  UseGuards,
} from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';
import { RolesGuard } from '../roles/roles.guard';
import { ApiCreatedResponse, ApiParam, ApiTags } from '@nestjs/swagger';
import { HolidaysService } from './holidays.service';
import { Roles } from '../roles/roles.decorator';
import { RoleEnum } from '../roles/roles.enum';

@UseGuards(AuthGuard('jwt'), RolesGuard)
@ApiTags('holidays')
@Controller({
  path: 'holidays',
  version: '1',
})
export class HolidaysController {
  constructor(private readonly holidaysService: HolidaysService) {}

  @Roles(RoleEnum.admin, RoleEnum.user)
  @Get()
  async getHolidays() {
    const holidays = await this.holidaysService.findHolidays();

    return holidays;
  }

  @Roles(RoleEnum.admin)
  @ApiCreatedResponse()
  @Post()
  async createHoliday(@Body() body: any) {
    return this.holidaysService.createHoliday(body);
  }

  @Roles(RoleEnum.admin)
  @ApiCreatedResponse()
  @Patch(':id')
  @ApiParam({
    name: 'id',
    type: String,
    required: true,
  })
  async updateHoliday(@Param('id') id: any, @Body() body: any) {
    return this.holidaysService.updateHoliday(id, body);
  }

  @Roles(RoleEnum.admin)
  @ApiCreatedResponse()
  @Delete(':id')
  @ApiParam({
    name: 'id',
    type: String,
    required: true,
  })
  async deleteDepartment(@Param('id') id: any) {
    return this.holidaysService.deleteHoliday(id);
  }
}
