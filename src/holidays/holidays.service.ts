import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Holiday } from './entities/holiday.entity';
import { Repository } from 'typeorm';

@Injectable()
export class HolidaysService {
  constructor(
    @InjectRepository(Holiday)
    private readonly holidaysRepository: Repository<Holiday>,
  ) {}

  async findHolidays() {
    const holidays = await this.holidaysRepository.find();

    return holidays;
  }

  async createHoliday(createHolidayDto: any) {
    const holiday = await this.holidaysRepository.create(createHolidayDto);

    return this.holidaysRepository.save(holiday);
  }

  async updateHoliday(id: any, updateHolidayDto: any) {
    const holiday = await this.holidaysRepository.update(id, updateHolidayDto);

    return holiday;
  }

  async deleteHoliday(id: any) {
    return this.holidaysRepository.delete(id);
  }
}
