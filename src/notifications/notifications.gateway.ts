import {
  WebSocketGateway,
  WebSocketServer,
  SubscribeMessage,
  MessageBody,
} from '@nestjs/websockets';
import { Server } from 'socket.io';
import { NotificationService } from './notifications.service';
import { forwardRef, Inject } from '@nestjs/common';

@WebSocketGateway(8001, { cors: '*' })
export class NotificationsGateway {
  @WebSocketServer() server: Server;

  constructor(
    @Inject(forwardRef(() => NotificationService))
    private readonly notificationsService: NotificationService,
  ) {}

  @SubscribeMessage('notification')
  async handleNotification(@MessageBody() payload: any): Promise<void> {
    const notification =
      await this.notificationsService.createNotification(payload);

    this.server.emit('notification', notification);
  }
}
