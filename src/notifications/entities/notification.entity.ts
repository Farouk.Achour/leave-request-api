import { Column, Entity, PrimaryGeneratedColumn } from 'typeorm';
import { EntityRelationalHelper } from '../../utils/relational-entity-helper';

@Entity({ name: 'notification' })
export class Notification extends EntityRelationalHelper {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  description: string;

  @Column()
  viewer: string;
}
