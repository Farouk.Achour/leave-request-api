import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { Notification } from './entities/notification.entity';

@Injectable()
export class NotificationService {
  constructor(
    @InjectRepository(Notification)
    private readonly notificationsRepository: Repository<Notification>,
  ) {}

  async createNotification(createNotificationDto: any) {
    const notification = await this.notificationsRepository.create(
      createNotificationDto,
    );

    return this.notificationsRepository.save(notification);
  }

  async getHRNotifications() {
    const notifications = await this.notificationsRepository.find({
      where: { viewer: 'HR' },
    });

    return notifications;
  }

  async getEmpNotifs(id: any) {
    const notifs = await this.notificationsRepository.find({
      where: { viewer: id },
    });

    return notifs;
  }
}
