import { Controller, Get, Param, UseGuards } from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';
import { RolesGuard } from '../roles/roles.guard';
import { ApiCreatedResponse, ApiParam, ApiTags } from '@nestjs/swagger';
import { Roles } from '../roles/roles.decorator';
import { RoleEnum } from '../roles/roles.enum';
import { NotificationService } from './notifications.service';

@UseGuards(AuthGuard('jwt'), RolesGuard)
@ApiTags('notifications')
@Controller({
  path: 'notifications',
  version: '1',
})
export class NotificationsController {
  constructor(private readonly notificationsService: NotificationService) {}

  @Roles(RoleEnum.admin)
  @Get()
  async getHRNotifications() {
    const notifications = await this.notificationsService.getHRNotifications();

    return notifications;
  }

  @Roles(RoleEnum.user)
  @ApiParam({
    name: 'id',
    type: String,
    required: true,
  })
  @Get(':id')
  findEmpNotifs(@Param('id') id: any) {
    return this.notificationsService.getEmpNotifs(id);
  }
}
