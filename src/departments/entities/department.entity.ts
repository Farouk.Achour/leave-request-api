import {
  Column,
  Entity,
  JoinColumn,
  OneToOne,
  PrimaryGeneratedColumn,
} from 'typeorm';
import { EntityRelationalHelper } from '../../utils/relational-entity-helper';
import { FileEntity } from '../../files/infrastructure/persistence/relational/entities/file.entity';
import { ApiResponseProperty } from '@nestjs/swagger';

@Entity({ name: 'department' })
export class Department extends EntityRelationalHelper {
  @PrimaryGeneratedColumn()
  id: number;

  @ApiResponseProperty({
    type: () => FileEntity,
  })
  @OneToOne(() => FileEntity, {
    eager: true,
  })
  @JoinColumn()
  photo?: FileEntity | null;

  @Column()
  name: string;

  @Column()
  chief: string;

  @Column()
  description: string;
}
