import { ApiCreatedResponse, ApiParam, ApiTags } from '@nestjs/swagger';
import { RoleEnum } from '../roles/roles.enum';
import {
  Body,
  Controller,
  Delete,
  Get,
  Param,
  Patch,
  Post,
  UseGuards,
} from '@nestjs/common';
import { Roles } from '../roles/roles.decorator';
import { AuthGuard } from '@nestjs/passport';
import { RolesGuard } from '../roles/roles.guard';
import { DepartmentsService } from './departments.service';
import { Department } from './entities/department.entity';

@UseGuards(AuthGuard('jwt'), RolesGuard)
@Roles(RoleEnum.admin)
@ApiTags('departments')
@Controller({
  path: 'departments',
  version: '1',
})
export class DepartmentsController {
  constructor(private readonly departmentsService: DepartmentsService) {}

  @Get()
  async getDepartments() {
    const departments = await this.departmentsService.findDepartments();

    return departments;
  }

  @Get(':id')
  @ApiParam({
    name: 'id',
    type: String,
    required: true,
  })
  findOne(@Param('id') id: Department['id']) {
    return this.departmentsService.findOneDepartment(id);
  }

  @ApiCreatedResponse()
  @Post()
  async createDepartment(@Body() body: any) {
    return this.departmentsService.createDepartment(body);
  }

  @ApiCreatedResponse()
  @Patch(':id')
  @ApiParam({
    name: 'id',
    type: String,
    required: true,
  })
  async updateDepartment(@Param('id') id: any, @Body() body: any) {
    return this.departmentsService.updateDepartment(id, body);
  }

  @ApiCreatedResponse()
  @Delete(':id')
  @ApiParam({
    name: 'id',
    type: String,
    required: true,
  })
  async deleteDepartment(@Param('id') id: any) {
    return this.departmentsService.deleteDepartment(id);
  }
}
