import { Get, Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Department } from './entities/department.entity';
import { Repository } from 'typeorm';

@Injectable()
export class DepartmentsService {
  constructor(
    @InjectRepository(Department)
    private readonly departmentsRepository: Repository<Department>,
  ) {}

  async findDepartments() {
    const departments = await this.departmentsRepository.find();

    return departments;
  }

  async findOneDepartment(id: any) {
    const department = await this.departmentsRepository.find({
      where: { id },
    });

    return department;
  }

  async createDepartment(createDepartmentDto: any) {
    const department =
      await this.departmentsRepository.create(createDepartmentDto);

    return this.departmentsRepository.save(department);
  }

  async updateDepartment(id: any, updateDepartmentDto: any) {
    const department = await this.departmentsRepository.update(
      id,
      updateDepartmentDto,
    );

    return department;
  }

  async deleteDepartment(id: any) {
    return this.departmentsRepository.delete(id);
  }
}
